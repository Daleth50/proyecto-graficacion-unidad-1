package graficacion;
import java.applet.*;
import java.awt.*;

public class Normalizacion extends Applet {

	private static final long serialVersionUID = 1L;
	int x=150,y=200,ancho=100,largo=130;
	double nx,ny,nancho,nlargo;
	
	public void init()
	{
		setSize(800, 600);
		nx=(double)x/800;
		ny=(double)y/600;
		nancho=(double)ancho/800;
		nlargo=(double)largo/600;
	}
	
	public void paint(Graphics g)
	{
		
		Dimension d=getSize();
		g.drawString(d.width+","+d.height, d.width-80, d.height-15);
		x=(int)(nx*d.width);
		y=(int)(ny*d.height);
		ancho=(int)(nancho*d.width);
		largo=(int)(nlargo*d.height);
		g.drawString(x+","+y+","+ancho+","+largo, d.width-120, 15);		
		g.setColor(Color.PINK);
		g.fillRect(x, y, ancho, largo);
	}

}

