package graficacion;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class BresenhamCirculo extends Applet {
	@Override
	public void init() {
		setSize(800,600);
		setBackground(Color.LIGHT_GRAY);
	}
	@Override
	public void paint(Graphics g) {
		drawBresenhamCircle(g);
	}
	public void drawPoint(Graphics g,int x, int y) {
		g.setColor(Color.blue);
		//System.out.println(x+" "+y);
		g.drawLine(x, y, x, y);
	}
	public void drawBresenhamCircle(Graphics g) {
		int radio=200,x,y;
		float d;
		x=100;
		int cont = 1;		
		y=radio;
		d=5/4-radio;
		drawPoint(g,x,y);
		while (cont <= 2) {
			System.out.print(x+" "+y+" ");
			while (y>x) {
				if (d<0) {
					d+=2*x+5;
					x++;
				} else {
					d+=2*(x-y)+5;
					x++;
					y--;
				}
				drawPoint(g,x,y);
				drawPoint(g,y,x);
				drawPoint(g,-x,y);
				drawPoint(g,-y,x);
				drawPoint(g,-x,-y);
				drawPoint(g,-y,-x);
				drawPoint(g,y,-x);
				drawPoint(g,x,-y);
				}
			System.out.println(x+" "+y);
			int bx=x,by=y;
			x=bx;
			y=by;
			cont++;
		}
			
	}

}
