package graficacion;
import java.awt.*;
import java.applet.*;
/**
 * @author daleth
 *
 */
public class Mapas2 extends Applet {
	Color PixMap[][];
	Color gray= Color.LIGHT_GRAY;
	Color red= Color.RED;
	public void init() {
		setSize(800,600);
		setBackground(Color.LIGHT_GRAY);
		PixMap= new Color[][] {
			{gray,gray,red,red,red,red,gray,gray},
			{gray,red,red,red,red,red,red,gray},
			{red,red,red,red,red,red,red,red},
			{red,red,red,gray,gray,red,red,red},
			{red,red,red,gray,gray,red,red,red},
			{red,red,red,red,red,red,red,red},
			{red,red,red,red,red,red,red,red},
			{red,red,red,gray,gray,red,red,red},
			{red,red,red,gray,gray,red,red,red},
			{red,red,red,gray,gray,red,red,red},			
			};
		

	}
	public void drawPM(Graphics g) {
		int x= 250, y= 100;
		for (int r = 0; r < PixMap.length; r++) {
			for (int c = 0; c < PixMap[r].length; c++) {
				g.setColor(PixMap[r][c]);
				g.drawLine(x,y, x, y);
				x++;				
			}
			x=250;
			y++;
		}
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.blue);
		drawPM(g);
	}
	
}
