package graficacion;

import java.awt.Graphics;
import javax.swing.JFrame;

public class Circle extends JFrame {
    int xCenter, yCenter, radius;

    public Circle() {
        xCenter = 150;
        yCenter = 150;
        radius = 50;
    }

    public static void main(String[] args) {
        Circle obj = new Circle();
        obj.setSize(300, 300);
        obj.setVisible(true);
    }

    public void paint(Graphics g) {
        int y = yCenter, x;
        while (y < (yCenter + radius / Math.sqrt(2))) {
            x = (int) Math.sqrt(Math.pow(radius, 2) - Math.pow(y - yCenter, 2)) + xCenter;
            g.drawRect(x, y, 1, 1);
            g.drawRect(300 - x, y, 1, 1);
            g.drawRect(x, 300 - y, 1, 1);
            g.drawRect(300 - x, 300 - y, 1, 1);
            g.drawRect(y, 300 - x, 1, 1);
            g.drawRect(300 - y, x, 1, 1);
            g.drawRect(y, x, 1, 1);
            g.drawRect(300 - y, 300 - x, 1, 1);
            y++;
        }
    }
}
