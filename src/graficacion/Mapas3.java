package graficacion;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Mapas3 extends Applet {
	int BitMap[];
	Color gray= Color.LIGHT_GRAY;
	Color red= Color.RED;
	public void init() {
		setSize(800,600);
		setBackground(Color.LIGHT_GRAY);
			BitMap= new int[] {
				0x3c,0x7e,0xff,0xe7,0xe7,
				0xff,0xff,0xe7,0xe7,0xe7
			};
		

	}
	public void drawBM(Graphics g) {
		int x= 250, y= 100;
		int mask=0x80;
		for (int r = 0; r < BitMap.length; r++) {
			for (int c = 0; c < 8; c++) {//el 8 es porque el dibujo es de 8 pixeles
				int v=mask>>c;//desplazamiento a la derecha a nivel de bits
				int res=BitMap[r]&v;//operacion and a nivel de bits
				if(res!= 0) {
					g.drawLine(x+c, y+r, x+c, y+r);
				}
				x++;
			}
			x=250;
			y++;
		}
	}
	public void paint(Graphics g) {
		g.setColor(Color.blue);
		drawBM(g);
	}
	
}

