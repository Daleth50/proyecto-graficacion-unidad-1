package graficacion;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Bresenham extends Applet {
public int xi=150,yi=250,xf=600,yf=-600;

	@Override
	public void init() {
		setSize(800,600);
		setBackground(Color.LIGHT_GRAY);
	}
	@Override
	public void paint(Graphics g) {
		drawBresenhamLine(g);
	}
	public void drawPoint(Graphics g,int x, int y) {
		g.setColor(Color.blue);
		System.out.println(x+" "+y);
		g.drawLine(x, y, x, y);
	}
	public void drawBresenhamLine(Graphics g) {
		int dx= xf-xi;
		int dy= yf-yi;
		int E = 2* dy - dx;
		int xIncrement= E + dx;
		int yIncrement= E - dx;
		int x= xi;
		int y = yi;
		while (x <= xf) {
			if(E > 0) {
				E = E+xIncrement;
			}else {
				E = E+yIncrement;
				y++;
			}
			x++;
			drawPoint(g, x, y);
		}
	}
}
