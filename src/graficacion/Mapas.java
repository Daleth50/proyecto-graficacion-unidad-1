package graficacion;
import java.awt.*;
import java.applet.*;
/**
 * @author daleth
 *
 */
public class Mapas extends Applet {
	char CarMap[][];
	public void init() {
		setSize(800,600);
		setBackground(Color.LIGHT_GRAY);
		CarMap= new char[][] {
			{' ',' ','*','*','*','*',' ',' '},
			{' ','*','*','*','*','*','*',' '},
			{'*','*','*','*','*','*','*','*'},
			{'*','*','*',' ',' ','*','*','*'},
			{'*','*','*',' ',' ','*','*','*'},
			{'*','*','*','*','*','*','*','*'},
			{'*','*','*','*','*','*','*','*'},
			{'*','*','*',' ',' ','*','*','*'},
			{'*','*','*',' ',' ','*','*','*'},
			{'*','*','*',' ',' ','*','*','*'},
			};
		

	}
	public void drawMap(Graphics g) {
		int x= 100, y= 200;
		for (int r = 0; r < CarMap.length; r++) {
			for (int c = 0; c < CarMap[r].length; c++) {
				g.drawString(CarMap[r][c]+"", x, y);
				x+=CarMap[0].length;
				
			}
			x=100;
			y+=CarMap[r].length;
		}
	}
	public void paint(Graphics g) {
		g.setColor(Color.blue);
		drawMap(g);
	}
	
}
